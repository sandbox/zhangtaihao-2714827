<?php
/**
 * @file
 * Manage extra custom config in hostmaster.
 */

/**
 * Implements hook_hosting_feature().
 */
function hosting_extra_config_hosting_feature() {
  $features['extra_config'] = array(
    'title' => t('Extra configuration'),
    'description' => t('Manage custom configuration to be added for Apache, Nginx, etc.'),
    'status' => HOSTING_FEATURE_DISABLED,
    'module' => 'hosting_extra_config',
    'group' => 'experimental',
  );
  return $features;
}
